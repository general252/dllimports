package dllimports

import (
	"syscall"
	"unsafe"
)

func StringToUTF16(str string) uintptr {
	data, err := syscall.UTF16PtrFromString(str)
	if err != nil {
		return 0
	}
	return uintptr(unsafe.Pointer(data))
}

func UTF16ToString(str []uint16) string {
	return syscall.UTF16ToString(str)
}
