package dllimports

import (
	"unsafe"
)

func StringToUTF8(str string) uintptr {
	if str == "" {
		return 0
	}

	temp := []byte(str)
	utf8StrArr := make([]uint8, len(temp)+1) // +1是因为Lazarus中PChar为0结尾
	copy(utf8StrArr, temp)

	return uintptr(unsafe.Pointer(&utf8StrArr[0]))
}

func UTF8ToString(src uintptr, strLen int) string {
	if strLen == 0 {
		return ""
	}

	str := make([]uint8, strLen)
	for i := 0; i < strLen; i++ {
		str[i] = *(*uint8)(unsafe.Pointer(src + uintptr(i)))
	}

	return string(str)
}
